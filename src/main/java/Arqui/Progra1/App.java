package Arqui.Progra1;

import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;

/**
 * Hello world!
 *
 */
public class App 
{
	public static final String PNG = "C:\\Users\\prott\\Documents\\TEC\\Semestre 9\\Arqui\\Files\\Input\\PNG Image.png";
	public static final String BMP = "C:\\Users\\prott\\Documents\\TEC\\Semestre 9\\Arqui\\Files\\Input\\BMP Image.BMP";
	public static final String GIF = "C:\\Users\\prott\\Documents\\TEC\\Semestre 9\\Arqui\\Files\\Input\\GIF Image.gif";
	public static final String XLSX = "C:\\Users\\prott\\Documents\\TEC\\Semestre 9\\Arqui\\Files\\Input\\Datos_Progra_1.xlsx";

	public static void main( String[] args ) throws IOException, InterruptedException
	{
		JSON json = new JSON("Hello", "This is the message body", XLSX);
		JSONObject finishedJson = json.buildJSON();

		if(finishedJson.get("Subject").toString().equals("Hello")) {
			if(JSON.fileType.contains("png") || JSON.fileType.contains("bmp") || JSON.fileType.contains("gif")) {
				try {
					MyRouteBuilder routeBuilder = new MyRouteBuilder();
					CamelContext context = new DefaultCamelContext();
					try {
						context.addRoutes(routeBuilder);
						context.start();
						PrintWriter writer = new PrintWriter("C:\\Users\\prott\\Documents\\TEC\\Semestre 9\\Arqui\\Files\\Input\\JSON.txt");
						writer.close();

						FileWriter file = new FileWriter("C:\\Users\\prott\\Documents\\TEC\\Semestre 9\\Arqui\\Files\\Input\\JSON.txt");
						file.write(finishedJson.toString());
						file.flush();
						file.close();
						Thread.sleep(2000);
						context.stop();
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if(JSON.fileType.contains("xlsx")) {

			}
			else {
				try {
					MyRouteBuilderMail routeBuilder = new MyRouteBuilderMail();
					CamelContext context = new DefaultCamelContext();
					try {
						context.addRoutes(routeBuilder);
						context.start();
						PrintWriter writer = new PrintWriter("C:\\Users\\prott\\Documents\\TEC\\Semestre 9\\Arqui\\Files\\Input\\JSON.txt");
						writer.close();

						FileWriter file = new FileWriter("C:\\Users\\prott\\Documents\\TEC\\Semestre 9\\Arqui\\Files\\Input\\JSON.txt");
						file.write(finishedJson.toString());
						file.flush();
						file.close();
						Thread.sleep(2000);
						context.stop();
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		else if(finishedJson.get("Subject").toString().equals("Hola")) {

		}
	}
}
