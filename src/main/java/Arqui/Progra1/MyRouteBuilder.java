package Arqui.Progra1;

public class MyRouteBuilder extends org.apache.camel.builder.RouteBuilder {
	
	@Override
	public void configure() throws Exception {
		from("file:C:/Users/prott/Documents/TEC/Semestre 9/Arqui/Files/Input?noop=true")
			.process(new MyProcessor())
			.to("file:C:/Users/prott/Documents/TEC/Semestre 9/Arqui/Files/Output");
	}

}
