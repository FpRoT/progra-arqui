package Arqui.Progra1;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.json.JSONObject;

public class MyProcessor implements Processor{

	public void process(Exchange exchange) {	
		System.out.println("Now processing the file");
		JSONObject jObj = new JSONObject(exchange.getIn().getBody(String.class));
		String path = (String) jObj.get("Path");
		if(JSON.fileType.contains("png") || JSON.fileType.contains("bmp") || JSON.fileType.contains("gif")) {
			try {

				File input = new File(path);
				File output = new File("C:/Users/prott/Documents/TEC/Semestre 9/Arqui/Files/Output/Image.jpg");

				BufferedImage image = ImageIO.read(input);
				BufferedImage result = new BufferedImage(
						image.getWidth(),
						image.getHeight(),
						BufferedImage.TYPE_INT_RGB);
				result.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
				ImageIO.write(result, "jpg", output);

			}  catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
